import "reflect-metadata";
import * as express from "express";
import axios, { AxiosResponse } from "axios"
import config from './config'
import * as Prometheus from 'prom-client'
import { ElrondStatus } from './status';
import {
  ElrondConnectedNodes,
  ElrondAcceptedBlock,
  ElrondConsensus,
  ElrondConsensusAcceptedBlock,
  ElrondLeaders,
  ElrondCPULoadPercent,
  ElrondCurrentBlockSize,
  ElrondCurrentRound,
  ElrondValidatorLive,
  ElrondMemoryHeapInUse,
  ElrondMemoryLoadPercent,
  ElrondMemoryTotal,
  ElrondMemoryUsedByGolang,
  ElrondMemoryUsedBySys,
  ElrondConsensusGroupSize,
  ElrondNetworkRecvBps,
  ElrondNetworkRecvPercent,
  ElrondNetworkSentBps,
  ElrondNetworkSentPercent,
  ElrondConnectedPeers,
  ElrondTransactionProcessed,
  ElrondSynchronizedRound,
  ElrondNetworkRecvBpsPeak,
  ElrondNetworkSentBpsPeak,
  ElrondNonce,
  ElrondNumNodesInShard,
  ElrondNumShardsWithoutMeta,
  ElrondRoundsPassedInCurrentEpoch,
  ElrondShardIdNormalized,
  ElrondConsensusStateAsInt,
} from './metrics'

const axiosInstance = axios.create({
  baseURL: config.ERD_NODE_URL,
  timeout: 1000
});

console.log(config.ERD_NODE_URL)

const MAP: any = {
  erd_connected_nodes: ElrondConnectedNodes,
  erd_count_accepted_blocks: ElrondAcceptedBlock,
  erd_count_consensus: ElrondConsensus,
  erd_count_consensus_accepted_blocks: ElrondConsensusAcceptedBlock,
  erd_count_leader: ElrondLeaders,
  erd_cpu_load_percent: ElrondCPULoadPercent,
  erd_current_block_size: ElrondCurrentBlockSize,
  erd_current_round: ElrondCurrentRound,
  erd_live_validator_nodes: ElrondValidatorLive,
  erd_mem_heap_inuse: ElrondMemoryHeapInUse,
  erd_mem_load_percent: ElrondMemoryLoadPercent,
  erd_mem_total: ElrondMemoryTotal,
  erd_mem_used_golang: ElrondMemoryUsedByGolang,
  erd_mem_used_sys: ElrondMemoryUsedBySys,
  erd_consensus_group_size: ElrondConsensusGroupSize,
  erd_network_recv_bps: ElrondNetworkRecvBps,
  erd_network_recv_bps_peak: ElrondNetworkRecvBpsPeak,
  erd_network_recv_percent: ElrondNetworkRecvPercent,
  erd_network_sent_bps: ElrondNetworkSentBps,
  erd_network_sent_bps_peak: ElrondNetworkSentBpsPeak,
  erd_network_sent_percent: ElrondNetworkSentPercent,
  erd_num_connected_peers: ElrondConnectedPeers,
  erd_num_transactions_processed: ElrondTransactionProcessed,
  erd_synchronized_round: ElrondSynchronizedRound,
  erd_nonce: ElrondNonce,
  erd_num_nodes_in_shard: ElrondNumNodesInShard,
  erd_num_shards_without_meta: ElrondNumShardsWithoutMeta,
  erd_rounds_passed_in_current_epoch: ElrondRoundsPassedInCurrentEpoch,
  erd_shard_id_normalized: ElrondShardIdNormalized,
  erd_consensus_state_as_int: ElrondConsensusStateAsInt,
}

let lastLabelNamesStr: string

const fetch = async () => {
  try {
    let metricsResponse: AxiosResponse<ElrondStatus> = (await axiosInstance.get<ElrondStatus>('/node/status'))

    if (config.DEBUG == "true") {
      console.log(metricsResponse.data)
    }

    const { metrics } = metricsResponse.data.data

    let labelNames: Prometheus.labelValues = {
      erd_app_version: metrics.erd_app_version,
      erd_chain_id: metrics.erd_chain_id,
      erd_latest_tag_software_version: metrics.erd_latest_tag_software_version,
      erd_public_key_block_sign: metrics.erd_public_key_block_sign,
    }

    if (labelNames.erd_public_key_block_sign == undefined) {
      return
    }

    // reset all existing metrics when labels change
    const labelNamesStr = JSON.stringify(labelNames)
    if (labelNamesStr !== lastLabelNamesStr) {
      Object.values(MAP).forEach(g => (g as any).reset())
      lastLabelNamesStr = labelNamesStr
    }

    // calc virtual values
    switch (metrics.erd_consensus_state) {
      case 'proposer':
        metrics.erd_consensus_state_as_int = 2
        break
      case 'participant':
        metrics.erd_consensus_state_as_int = 1
        break
      case 'not in consensus group':
        metrics.erd_consensus_state_as_int = 0
        break
      default:
        metrics.erd_consensus_state_as_int = -1
    }
    switch (metrics.erd_shard_id) {
      case 4294967295: // metachain
        metrics.erd_shard_id_normalized = -1
        break
      default:
        metrics.erd_shard_id_normalized = metrics.erd_shard_id
    }

    // set values
    Object.entries(MAP).forEach(([key, gauge]) => {
      const v = (metrics as any)[key]

      if (typeof v !== 'undefined') {
        (gauge as any).set(labelNames, v)
      }
    })
  } catch (err) {
    console.error(`Error fetching!`)
    console.error(err)
  }

  setTimeout(fetch, 2000)
}
fetch()

/**
 * Express HTTP API
 */
const app = express();


app.get('/metrics', (req: express.Request, res: express.Response) => {
  res.set('Content-Type', Prometheus.register.contentType)

  const { prefix } = req.query

  let str = Prometheus.register.metrics()

  if (prefix) {
    str = str.replace(/\nerd_/gm, `\n${prefix}_`)
  }

  res.end(str)
})

app.listen(config.HTTP_PORT, () => {
  console.log(`Metrics HTTP Server started on port ${config.HTTP_PORT}!`);
});

