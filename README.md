# Elrond node exporter

**Forked from https://gitlab.com/bliiitz-corp/elrond/elrond-node-exporter**

This requests `/node/status` of an Elrond node and exposes the useful metrics for [Prometheus](https://prometheus.io/) ingestion. See `index.ts` for the list of exposed metrics.

It also exposes the following non-standard metrics:

* `erd_consensus_state_as_int` - a number indicating whether node is in the consensus group. `0` = not in group, `1` = participant, `2` = proposer/leader, `-1` = unknown state

# How to use

To get it working, first install Docker on your node machine:

```shell
apt install -y docker.io
```

Now run it:

```shell
$ docker run --net=host registry.gitlab.com/hiddentao/elrond-node-exporter:v2.0
```

Visit `http://<node server ip>:8081/metrics` to see the results. You can change the metric label prefix by appending `?prefix=<your chosen prefix>` to the URL.

To have it always running, even when server restarts:

```shell
$ docker run --net=host -d --restart always registry.gitlab.com/hiddentao/elrond-node-exporter:v2.0
```

These are the env vars that the Docker container uses by default:

- `HTTP_PORT`: `8081` (Port which exposes prometheus metrics endpoint)
- `ERD_NODE_URL`: `http://localhost:8080` (Root url of elrond node)
- `DEBUG`: `false` (print each request response of elrond node)

